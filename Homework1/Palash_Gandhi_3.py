import sys

"""
Foundations of Intelligent Systems - Homework 1 Question 3
This file provides a method called friendsOfFriends to find the shortest path
between 2 friends, if one exists.
Author: Palash Gandhi (pbg4930)
"""

nodes = {}
paths = []


class Node:
    '''
    This is the class used to represent a node/vertex in the graph.
    '''

    def __init__(self, name):
        """
        Constructor for node data structure.
        :param name: The name of the person.
        """
        self.children = []
        self.name = name

    def set_children(self, child_list):
        """
        Method that sets the children of a node. Creates a new child node if 
        one does not exist.
        :param child_list: The list of names of the children of the node.
        """
        for child in child_list:
            if child in nodes:
                self.children.append(nodes[child])
            else:
                child_node = Node(child)
                nodes[child] = child_node
                self.children.append(child_node)

    def __repr__(self):
        return "Name: " + self.name


def create_tree(file):
    """
    Method that creates the tree structure on which the search is run.
    :param file: The name/path of the file based on which the tree is 
    constructed
    """
    with open(file) as f:
        lines = f.readlines()
        for line in lines:
            names = line.strip().replace(" ", "").split(",")
            name = names[0]
            if name in nodes:
                node = nodes[name]
            else:
                node = Node(name)
            node.set_children(names[1:])
            nodes[name] = node


def bfs(begin_vertex, end_vertex):
    """
    Method that runs a Breadth-First search on the constructed tree and
    finds the shortest path between the 2 inputs.
    :param start: Start vertex (person's name)
    :param end: End vertex (person's name) 
    """
    queue = [(begin_vertex, [begin_vertex])]
    while queue:
        (current, path) = queue.pop(0)
        for child in set(current.children):
            if child not in path:
                new_path = path + [child]
                if child.name == end_vertex.name:
                    paths.append(new_path)
                else:
                    queue.append((child, new_path))


def find_min_path():
    """
    Helper function to return the shortest path among all found paths.
    :return: min_path: The shortest path that is found.
    """
    min_cost = -1
    min_path = None
    for path in paths:
        if min_cost == -1:
            min_cost = len(path)
            min_path = path
        else:
            if len(path) <= min_cost:
                min_cost = len(path)
                min_path = path
    return min_path


def friendsOfFriends(f, name1, name2):
    """
    Main function to call that creates the tree, runs a BFS on the tree and 
    prints the shortest path, if one exists.
    :type f: file
    :type name1: str
    :type name2: str
    :rtype: list
    """
    create_tree(f)
    if name1 in nodes and name2 in nodes:
        bfs(nodes[name1], nodes[name2])
        min_path = find_min_path()
        if min_path is None:
            print "There is no path from {0} to {1} in {2}".format(name1, name2,
                                                                   f)
            return
        solution = []
        for vertex in min_path:
            solution.append(vertex.name)
        print solution
    else:
        print "There is no path from {0} to {1} in {2}".format(name1, name2, f)


def main():
    args = sys.argv[1].split(",")
    file_path = args[0]
    source = args[1]
    target = args[2]
    friendsOfFriends(file_path, source, target)


if __name__ == '__main__':
    main()
