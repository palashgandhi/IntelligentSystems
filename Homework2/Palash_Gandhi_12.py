"""
Foundations of Intelligent Systems - Homework 1 Question 3
This file provides a method called condIndependence to find the conditional 
independence between 2 variables, given a third.
Author: Palash Gandhi (pbg4930)
"""

nodes = {}

class Node:
    '''
    This is the class used to represent a node/vertex in the graph.
    '''

    def __init__(self, name):
        """
        Constructor for node data structure.
        :param name: The name of the person.
        """
        self.parents = []
        self.children = []
        self.edges = []
        self.name = name

    def set_children(self, child_list):
        """
        Method that sets the children of a node. Creates a new child node if 
        one does not exist.
        :param child_list: The list of names of the children of the node.
        """
        for child in child_list:
            if child in nodes:
                self.children.append(nodes[child])
            else:
                child_node = Node(child)
                nodes[child] = child_node
                self.children.append(child_node)

    def set_parent(self, parent_list):
        """
       Method that sets the parent of a node. Creates a new parent node if 
       one does not exist.
       :param parent_list: The list of names of the parent of the node.
       """
        for parent in parent_list:
            if parent in nodes:
                self.parents.append(nodes[parent])
            else:
                parent_node = Node(parent)
                nodes[parent] = parent_node
                self.parents.append(parent_node)

    def __repr__(self):
        return str(self.name)


def create_tree(file):
    """
    Method that creates the tree structure on which the search is run.
    :param file: The name/path of the file based on which the tree is 
    constructed
    """
    with open(file) as f:
        lines = f.readlines()
        for line in lines:
            names = line.strip().replace(" ", "").split(",")
            child_name = names[0]
            if child_name in nodes:
                child = nodes[child_name]
            else:
                child = Node(child_name)
                nodes[child_name] = child
            child.set_parent(names[1:])
            for parent_name in names[1:]:
                if parent_name in nodes:
                    parent = nodes[parent_name]
                else:
                    parent = Node(parent_name)
                    nodes[parent_name] = parent
                parent.set_children([child_name])


def _get_ancestors(node):
    """
    Helper function to recursively find all the ancestors of the node.
    :param node: The node whose ancestors are to be found
    :return: ancestors: List of all ancestors of the node
    """
    ancestors = []
    ancestors.append(node)
    if len(node.parents) > 0:
        for parent in node.parents:
            if parent not in ancestors:
                ancestors.append(parent)
            for ancestor in _get_ancestors(parent):
                if ancestor not in ancestors:
                    ancestors.append(ancestor)
    return ancestors

def get_ancestors(node):
    """
    Function to get ancestors of a node
    """
    ancestors = _get_ancestors(node)
    return ancestors


def build_ancestor_graph(list_of_nodes):
    """
    Builds the ancestral graph for the given list.
    """
    ancestor_nodes = []
    for node in list_of_nodes:
        if nodes[node] not in ancestor_nodes:
            ancestor_nodes.append(nodes[node])
            for ancestor in get_ancestors(nodes[node]):
                if ancestor not in ancestor_nodes:
                    ancestor_nodes.append(ancestor)
    return ancestor_nodes


def draw_undirected_edge(source, target_nodes):
    """
    Helper function to connect a source and a list of target nodes with an 
    undirected edge
    :param source: Edge from node
    :param target_nodes: List of nodes containing the targets
    """
    for target_node in target_nodes:
        if target_node != source:
            if source not in target_node.edges:
                target_node.edges.append(source)
            if target_node not in source.edges:
                source.edges.append(target_node)


def moralize(ancestor_graph):
    """
    Moralizes the graph. Connects the parents of all nodes with an undirected
    edge.
    """
    for node in ancestor_graph:
        for parent in node.parents:
            draw_undirected_edge(parent, node.parents)
    return ancestor_graph


def disorient(moralized_graph):
    """
    Converts all edges from directed to undirected. 
    """
    for node in moralized_graph:
        for connected_node in node.parents + node.children:
            if connected_node in moralized_graph:
                node.edges.append(connected_node)
    return moralized_graph


def delete_givens(disoriented_graph, givens):
    """
    Deletes the givens from the graph and removes all edges associated with 
    that node.
    :param disoriented_graph: 
    :param givens: 
    :return: 
    """
    for given in givens:
        given_node = nodes[given]
        for node in disoriented_graph:
            while given_node in node.edges:
                node.edges.remove(given_node)
        disoriented_graph.remove(given_node)
    return disoriented_graph


def path_exists(begin_vertex, end_vertex):
    """
    Method that runs a Breadth-First search on the constructed tree to check 
    if a path exists between the 2 vertices.
    :param start: Start vertex
    :param end: End vertex 
    """
    queue = [(begin_vertex, [begin_vertex])]
    while queue:
        (current, path) = queue.pop(0)
        for child in set(current.edges):
            if child not in path:
                new_path = path + [child]
                if child.name == end_vertex.name:
                    return True
                else:
                    queue.append((child, new_path))
    return False


def determine_dependence(X, Y):
    """
    Checks if a path exists between any of the nodes of X to any of the nodes
    of Y. Returns False if a path exists (conditionally dependent) else, True.
    :param X: 
    :param Y: 
    :return: 
    """
    for node_x_name in X:
        node_x = nodes[node_x_name]
        for node_y_name in Y:
            node_y = nodes[node_y_name]
            if path_exists(node_x, node_y):
                return False
    return True


def condIndependence(f, X, Y, Z):
    """
    Main function to be called to determine the conditional independence.
    :param f: file
    :param X: list
    :param Y: list
    :param Z: list
    """
    create_tree(f)
    ancestor_graph = build_ancestor_graph(X+Y+Z)
    moralized_graph = moralize(ancestor_graph)
    disoriented_graph = disorient(moralized_graph)
    final_erased_graph = delete_givens(disoriented_graph, Z)
    print determine_dependence(X, Y)




def main():
    condIndependence("input.txt", ['D'], ['E'], ['A'])


if __name__ == '__main__':
    main()
