def _get_mean(list_values):
    """
    Given a list of numerical values, returns the mean of the values.
    :param list_values: 
    :return: 
    """
    sum_elements = 0
    for item in list_values:
        sum_elements += item
    return sum_elements/len(list_values)


def train(temp, cnt):
    """
    Trains the model on the training set.
    :param temp: List of temperatures for the X axis. 
    :param cnt: List of counts for the Y axis
    :return: b0 and b1
    """
    temperature_mean = _get_mean(temp)
    count_mean = _get_mean(cnt)
    numerator, denominator = 0, 0
    for i in range(len(temp)):
        numerator += (temp[i] - temperature_mean) * (cnt[i] - count_mean)
        denominator += (temp[i] - temperature_mean) ** 2
    b1 = numerator / denominator
    b0 = count_mean - (b1 * temperature_mean)
    return b0, b1


def test(temp, cnt, b0, b1):
    """
    Tests the model on the testing set.
    :param temp: List of temperatures for the X axis
    :param cnt: List of counts for the Y axis
    :param b0: The value for b0 obtained from the train method
    :param b1: The value for b1 obtained from the train method
    :return: mean_squared_error
    """
    mean_squared_error_sum = 0
    for i in range(len(cnt)):
        mean_squared_error_sum += (cnt[i] - (b0 * temp[i] + b1)) ** 2
    return mean_squared_error_sum / len(cnt)

