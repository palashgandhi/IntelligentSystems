"""
Driver.py
author: Yuxiao Huang
description: Load data, train, test, and plot the linear model (cnt = b1 * temp + b0)
"""


from solution import *
import matplotlib.pyplot as plt
import numpy as np


# Load data from file
def load_data(dataset_name, col_temp, col_cnt):
    with open(dataset_name, 'r') as dataset_name:
        lines = dataset_name.readlines()

        # Get temp and cnt
        temp = []
        cnt = []
        # Start from lines[1] to skip the header
        for i in range(1, len(lines)):
            line = lines[i].strip()
            arr = line.split(',')
            temp.append(float(arr[col_temp]))
            cnt.append(float(arr[col_cnt]))

    return np.array(temp), np.array(cnt)


# Plot the data and linear model
def plot(temp, cnt, b0, b1, figure):
    fig, ax = plt.subplots()
    y_hat = b1 * temp + b0
    ax.plot(temp, y_hat, color = 'red')
    ax.scatter(temp, cnt)
    plt.xlabel('temp')
    plt.ylabel('cnt')
    plt.savefig(figure)


# main function
if __name__=="__main__":
    # Training set
    training_set_file = "hour.csv";
    # Testing set
    testing_set_file = "day.csv";

    # Training result
    training_result_figure = "training_result.pdf";
    # Testing result
    testing_result_figure = "testing_result.pdf";
    # mse
    mse_file = "mse.txt";

    # Load data from training_set_file
    temp, cnt = load_data(training_set_file, 10, 16)

    # Train the linear model on training data
    b0, b1 = train(temp, cnt)

    # Plot training_result_figure
    plot(temp, cnt, b0, b1, training_result_figure)

    # Load data from testing_set_file
    temp, cnt = load_data(testing_set_file, 9, 15)

    # Test the linear model on testing data
    mse = test(temp, cnt, b0, b1)

    # Output mse
    with open(mse_file, 'w') as mse_file:
        mse_file.write(str(mse) + '\n')

    # Plot testing_result_figure
    plot(temp, cnt, b0, b1, testing_result_figure)
